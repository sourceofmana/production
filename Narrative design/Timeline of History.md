# Timeline of Aemil


## 1st Era - The Hantu Era
### Age of Origin (1E0-1E1000)
All elements exist in the Ether and float in a state of chaos, sometime coalescing into planets and stars. Mana and Kaore are always essential elements of this process. 

Extremely high concentrations of Mana exposed to the dense chaos of other elements result in the formation of a planet that will one day be known as Aemil.

After floating dormant in the Ether for an unfathomably long time, carrying little life in the form of aquatic bacteria, a new tendril of life finds its way to Aemil from the quiet darkness of the universe. A colossal nebula of pure Mana energy travelling nearby makes contact with the fated planet and is magnetised by its unusual mineral core. A reaction is set in motion. With a loud resonating sound and a flash of bright light, Aemil quickly absorbs the entire cloud of Mana into its core.

Everything grows quiet. Over a short period of time, the simple life forms of Aemil begin to change and are animated by a new desire to grow. 

A particular life form evolved at unusual speed and took the shape of an imposing tree -the first Hakuturi, or Mana Tree. Unlike other organisms, this tree seemed deeply linked with Mana itself, using it directly as sustenance rather than sunlight or regular nutrition. Thanks to this strong connection to Mana, the tree grew to almost touch the sky and its seeds spread in every direction.

The Hakuturi grow long root systems connecting to each other and embracing the whole world. Their existence draws Mana from the core of Aemil and spreads it like pollen in the wind, creating a high concentration of Mana on the surface where living creatures can interact with it directly.

#### The Source (1E0)
"A spark, something unknown and unfathomable."

#### Birth of the Hantu (~1E500)
On the surface of Aemil, life is plentiful and thriving. The abundance of Mana allows various creatures to quickly evolve and adapt better to their environment. As Mana helps them evolve and heal easily, most creatures are unburdened by dangers of disease or injury. Thus, they prefer remaining in smaller numbers, allowing for greater prosperity in the ecosystem.

The Hantu are the first to develop a high level of intelligence and begin forming complex societal structures. They spread across all lands and live harmoniously with only occasional wars. They enjoy good relations with the Hakuturi, the only other sentient creatures on Aemil. Over time, the Hantu harness Mana and master all forms of magic. They build an empire across Aemil and cohabit with the Hakuturi in large groves, often along with other trees to form immense forests covering most of the surface of the world. The Hakuturi begin to speak to the Hantu and adopt their language to communicate. They form symbiotic societies where the Hakuturi are revered by the Hantu as wise mentors in tune with the lifeblood of the world, and the Hantu are seen by the Hakuturi as the stewards of Aemil.

### First Age of Mana (1E1000-1E5403)
The Hantu and Hakuturi live in perfect harmony for thousands of years. The presence of Mana rises immensely allowing the Hantu to live long, healthy, and peaceful lives. They learn to harness and manipulate Mana and invent the first forms of magic. They lead easy, slow lives in the abundance of their forests. Instead of building new structures, they move the earth itself as well as rivers, lakes, islands, and mountains.

They carve an island in the shape of a tree and hide there their recorded wisdom. They carve an island in the shape of a skull to hide something dangerous, but they do not leave records of what they hid. The primal magic practiced in the Hantu Era becomes largely forgotten later on and the exact location of their hidden secrets becomes impossible to find.

The Hantu build no cities or kingdoms or nations. They evolve to be perfectly suited to the wild and make comfortable and complex homes out of trees, caves, and other features of the land. The Hakuturi prosper and build a rich culture alongside the Hantu. Their ability to communicate instantly with each other creates an interconnected if sparsely populated world.

#### Birth of the Tritans (~1E4000)
As the Hantu walk the land and dwell among the trees, young tadpoles in a deep trench of the Tritan Ocean are beginning to grow into exceptionally intelligent creatures. Within only a few generations they form village communities and develop languages. The exceptionally adaptable Tritan race achieves sentience.

### Age of Ker Ys (1E5403-1E5800)
All waters become the domain of the Tritans. As the Hantu lead slow, largely peaceful lives on land, the Tritans live in fast-paced warrior societies. Great Tritan kingdoms are formed. The city of Ker Ys is the center of power where all Tritan kings meet as equals, it is the largest and richest city on Aemil.

Tritans master the art (and science) of magic and use it to their advantage, learning to channel Mana out of the deeper parts of the ocean floor. However, the isolated Mana generated at those depths cannot benefit from the constant flow created by the Hakuturi. The high concentrations of Mana present at the depths of the ocean begin to decay into Kaore at a rapid pace.

The Tritans dwelling in the deeper parts of the ocean are affected by the growing Kaore and become more aggressive, beginning to desire greater power. They wage wars on the Hantu for the first time, pushing them away from coastal areas and taking control of waterways. The Hantu, however, are formidable foes and hold most of their ground, stalling the war for almost 400 years.

#### The First Event (1E5800)
A vicious Tritan king, driven mad by Kaore, devises a mass attack on all Hantu settlements on the western coast of Ancea. Arising from the Tritan Ocean, the largest army ever assembled descends upon the Hantu and slaughters hundreds of thousands. Millions more die from the sudden surge in Kaore generated by the mass killing. Enough Kaore is formed that it begins to spread death in new material forms. The Tritans begin to lose their reason, fighting each other and getting lost in the unfamiliar lands. The Hantu are affected by a terrible plague that spreads over the winds to every corner of Aemil, killing almost every living Hantu in existence.

The legions of Tritan soldiers lost on the lands of Ancea are forced to settle away from the sea for the first time in their history. Their wits diminished by the mind-degrading effects of Kaore, they find themselves set back in their evolution and wander the land aimlessly for a long time. Their highly adaptable bodies begin to adjust to the new environment and they slowly take over abandoned Hantu camps. They learn to respect the Hakaturi, which they understand to be creatures of great magical power. They know that the Hantu can speak with the Hakaturi, but the trees have fallen silent and will not talk following the First Event. They seem neither hostile nor friendly, just uninterested in interacting with the world.

###  Age of Landwalkers (1E5801-1E7000)
After the First Event, very few Hantu survive. They hide in the deepest forests and mountains trying to escape the deadly plague carried in the air itself. They use protective spells to ward off some areas and make them safe, but they continue to clash with Tritans who seek them out and - sometimes inadvertently - bring the plague with them. Over the course of a few centuries, the last Hantu die out leaving only some traces of their history, like the tradition of carving Soul Menhirs from Zielite. Their existence is usually unknown outside scholarly circles and some believe that they are creatures of legend.

With the Hantu gone, the continent of Aurora remained largely uninhabited with the exception of small Tritan fishing villages along the south-western coast. Ancea's new inhabitants are referred during this age as Landwalkers. The Kaore-affected Tritans left behind from the war, who have now devolved into semi-feral land creatures.

#### Birth of the Humans and Ukar (1E6000?)
The Landwalkers split into two groups representing the most significant environments they have found: the mountains and the forests. The mountain tribes want to settle deep in the high rocks of the Tempus Mountains, where they will be safe from attacks and they can mine resources to make them prosperous. The forest tribes prefer lower grounds, where they can grow more food and expand more easily. Over time, the mountain tribes evolve into the Ukar and the forest tribes into the Humans. These two groups eventually find balance with the remaining Tritans that continue to dwell in shallower waters and along the coasts. The continent of Ancea becomes the first home of the new land dwellers. Over the centuries they begin to regain their intelligence and continue to evolve guided by Mana.

## 2nd Era - The Ancean Era
### Founding of Keshlam and Mangarron (2E1)
Legend says that the first Human and the first Ukar cities are founded on the same day. Their day of founding marks the first year of the Second Era.

Ever since their arrival on Ancea, the Landwalkers live a nomadic lifestyle and take what the land has to give. For hundreds of years, semi-permanent camps exist throughout Ancea but the young evolving races prefer to never settle in one place for too long.

The two races have now been split in two different environments for over 1000 years, exposed to the heavy flow of Mana that the Hakuturi continue to enable. This causes the two races to evolve very different features and become fully distinct from each other and the original Tritans. At this stage, both races begin to grow closer communal ties and thus select two camps to become permanent cities and places of gathering. These will eventually become the Human castle and town of Keshlam and the Ukar mountain fortress of Mangarron.

### Age of the Platinum Kingdom (2E201-2E1264)
Hailing from the higher ranks of the City of Keshlam, the young Victorio inherits the family lands south of the city. The site has recently been discovered as a rich source of Platinum Ore and Victorio becomes very wealthy. In 2E201, the old Chief of Keshlam dies and a customary election is held. Such is the popularity and influence of Victorio that he is proclaimed the first King of Humans. He names his domain the Platinum Kingdom, and himself the Platinum King. His rule lasts over 60 years and under his leadership Keshlam prospers into the greatest city that ever existed.

The Platinum Kingdom eventually receives the fealty of all Human communities on Ancea. The Ukar continue to live in separate independent settlements carved deep into the Tempus Mountains. Both populations remain small and naturally self-contained, a trait typical of creatures whose life is lived in a Mana-rich environment. The Platinum Kingdom, however, creates various social classes and a complex social hierarchy that is previously unknown and will become typical of Humans over time.

#### First Mana Storm (2E470-2E480)
A mysterious increase in Mana flow over Aemil causes all nature to grow at an accelerated rate. Records from this period are extremely unusual, with descriptions of bizarre feats of magic, erratic behaviour, illogical stories and a general sense of whimsical madness. It is believed that happiness washed over all people and throughout the storm everyone was affected by an unusually positive mood.

The sentient races become increasingly restless and begin to desire new settlements and different lifestyles. The current Platinum King, Victorio III, is inspired to expand his kingdom and found new cities.

It is believed by Ukar scholars that the First Mana Storm caused the sentient races of Aemil to develop ambition and desire for power.

#### Founding of Hurnscald (2E477)

The Platinum Kingdom expands eastwards, constructing a walled timber fortress containing its town named Hurnscald. The town starts out as a garrison and a port to the Aemilian Sea, but soon rich minerals are discovered in the mountains nearby, and mining becomes a great industry in this area.

#### Founding of Tulimshar (2E489)

The vast grasslands of Tonori are inhabited by the nomadic Zuni tribes, the earliest Humans to evolve on Ancea. Their rich culture and unique goods encourage the Platinum Kingdom to grow closer ties. A hospitable patch of coastline is selected to found a colony, which is named Tulimshar. The colony receives a significant investment in its early years and quickly grows into a successful trading centre.

#### Founding of Nivalis (2E496)

The cold north is sparsely populated by Human and Ukar alike, living in small nomadic tribes known as Barbarians. The expansionist King Victorio III launches an expedition and finds an elevated plateau, near the mountain passes that lead into Kaizei but also close enough to the Aemilian Sea. Here, he founds the City of Nivalis. A refuge from the cold and a centre of trade for the whole region.

#### Birth of the Kralogs (2E500?)

The first Ukar note that the mountains they inhabit do not cover as much land as the lands where the Humans live. A group of adventurous Ukar set forth in the early decades of the Ancean Era to find new elevated lands to settle. They navigate the cold seas of the north and land on a volcanic island which they name the Land of Fire. The climate of the island is far from ideal and the settlers are tested to their extreme, but they survive and more generations are born. Over time, they adapt to the temperature extremes and harsh living conditions of the Land of Fire and become the resilient race now known as Kralogs.

#### Founding of Candor (2E551)

The island of Candor has been used as an outpost for a long time, but it is considered too dangerous to settle. This changes with the rise of Hurnscald on the nearby coast and a small outpost is established on the island, connected to Hurnscald, Tulimshar, and Nivalis. The island's mysterious cave system, thought to have been built by mysterious ancient inhabitants of Aemil, is thought to be breached as some large chambers are uncovered. These caves begin to be used by the Platinum King as an arena to host yearly war games. Scholars believe the cave system is much deeper, but the ancient Hantu wards make every side of the cave seem like a dead end.

### Reign of Queen Platyna the Red (2E1199-2E1265)

Queen Platyna XII, the Red Queen, is the last monarch of the Platinum Kingdom. During her reign, she severely limits the power of her advisors and insists on directly controlling her kingdom, despite rarely making time for it. She is ultimately more interested in keeping her gardens. Due to her unwillingness to cooperate with her council, the kingdom's economy begins to crumble. After 40 years of mismanagement, the now older Red Queen is despised by all her subjects. Over the years, she grows spiteful and sadistic, ordering public executions for petty crimes and demanding ever higher taxes.

#### The Blue Revolution (2E1265)

Tired of the Queen's neglect for her people, the council decides to seize power and depose her. On a spring day, during one of the Queen's long absences from court, Chancellor Benjamin Frost convinces the entire council to support an immediate takeover. He proclaims the Republic of Ancea and appoints himself Lord Prince of the Republic. The Royal Guard is ordered to shut the gates of Keshlam and not allow the Queen back into the city. This event becomes known as the Blue Revolution, taking its name from the uniform colours of the Chancellor, and marks the end of the Platinum Kingdom. Frost maintains his title for eighteen months and is eventually deposed by the very council that supported him during the Revolution. Rather than naming a successor, the council names itself the Senate of Ancea, the collective ruler of the Republic.

The Red Queen, now deposed, escapes to the Arikel Savannah in the Tonori region, which had always remained independent from the politics of Ancea. She attempts to recreate a small kingdom, but pays little attention to her faithful followers and her small settlement is abandoned by the time of her death. She dies in the camp-city of Modan where local chieftains choose to bury her as royalty.

### Age of the Republic of Ancea (2E1265-2E1979)

The Republic is the first to bring democracy to Aemil. It reunites the city-states of Keshlam, Hurnscald, Tulimshar, and Nivalis into one nation where all citizens participate in decision-making. The system grows increasingly bureaucratic, but it successfully endures for over 700 years, marking a mostly prosperous and happy period in the history of Ancea.

#### End of the Republic (2E1980)

In its later years, the Republic grows increasingly corrupt. Political factions begin to seek the destruction of their rivals and wealthy citizens influence most decisions. The four cities of the Republic become increasingly distrustful of each other. In 2E1979, the city of Hurnscald declares its independence along with its historic surrounding territory. In 2E1980, Nivalis and then Tulimshar follow. Alone in the Republic, the rulers of Keshlam proclaim its dissolution, and thus all four cities become independent.

### The Great Quake (2E1981)

The caste of Sorcerers that rule Tulimshar at this time attempt to find what they believe to be the inner source of Mana inside the world of Aemil. They devise powerful magic constructs powered with Mana Crystals to dig deep into the earth. At an unknown depth, something is triggered. Two-thirds of the vast ocean previously dividing the continents of Ancea and Aurora sink inwards into the core of the world. Much of the Tritan civilization is instantly extinguished, with the exception of those on the Oranye Isles and a few sheltered bays. The Aemilian Sea is now a smaller body of water almost fully surrounded by the two continents of Ancea and Aurora.

### Age of Exploration (2E1981-2E1999)

Navigators find new lands to the East. The faraway Oranye Isles and distant Aurora seem to have moved closer, creating a calmer sea with shorter distances to cover. A renewed spirit of adventure inspires the people of Ancea. Adventurers, as well as refugees from the revolutionary war, are given ships and organized by the City-States into expedition parties and sent to explore the untamed lands across the Aemilian Sea. The Fleet of Aurora is the largest expedition and becomes the most well-known.

#### Discovery of Ker Ys (2E1989)

The City of Ker Ys has existed since the Hantu Era and continues to serve as the capital city of the whole Aemilian Sea and the Tritan Ocean beyond. It stands nested in the low-lying Oranye Isles, partially submerged and densely populated. Following the Great Quake, the vast landscape of Tritan settlements in the far sea is largely lost and the Oranye Isles stand as the main Tritan homeland. Once far in the middle of the Ocean, the Isles have moved closer to the other lands of Aemil and Ker Ys begins to receive its first visitors.

## 3rd Era - The Auroran Era

### Founding of Esperia (3E1)

The Fleet of Aurora travels through the Oranye Isles and continues eastward. It reaches the coast of Aurora and encounters a small Tritan swamp village built on reeds. The few inhabitants are welcoming to the explorers and explain how the swamp is safer as the dangerous beasts of Aurora stay away from it. Together, the two groups build over the existing settlement and create a town on the water that dominates the surrounding landscape. They have great hopes for their new city and name it Esperia. Over the years the city grows into a great trading hub and a highly desired city due to its pleasant climate and beautiful buildings.

### Age of the City-States (3E1-3E2476)

#### Founding of Artis (3E59)

### Age of Piracy (3E2477-3E2897)

### Second Mana Storm (3E2897-3E2909)

### Mana War (3E2905-3E2941)

### The Grand Calamity (3E2941)

#### Birth of the Raijin (3E2941)

#### Destruction of Keshlam (3E2941-3E2945)

### Age of Kaore (3E2945-3E3465)

Previously a minor force on Aemil, Kaore usually only forms when Mana stagnates and cannot flow. It is also generated by death, and the events of the Mana War cause so many deaths that they permanently raise the concentration of Kaore in the world. This leads to a 530-year dark age where war, disease, and famine often afflict the people of Aemil. This period culminates in the Kaore Storm, an event that ushers in the beginning of the Fourth Era.

## 4th Era - The Fourth Era

### Kaore Storm (4E1)

The high concentration of Kaore grows ever stronger as it feeds on the death and destruction that it creates. Eventually, it can no longer grow due to the strong Mana presence on Aemil and it implodes, causing an event known as the Kaore Storm. It lasts only a few days (unlike Mana Storms which last some years) and causes every living creature to fall into a deep sleep. During this time, most people remain unharmed. Once the storm is over, there seems to be no detectable presence of Kaore anywhere. Mana is also entirely absent.

### Second Age of Mana (4E1-Present)

As Mana slowly returns to Aemil, its ability to fill the world as it once did is impaired. The Hakuturi no longer connect the world and Kaore threatens to return as the leading force of magic.
