## Prologue

You awaken under a palm tree with an empty bowl of water next to you. You wonder if you might have passed out from the heat of the desert...

You know where you are. The famous city of Tulimshar in the Tonori Desert. The scorching heat of the sun irradiates its great sandstone walls.

A young woman with green hair and a pink dress smiles at you as she brings you fresh water. Thankfully the town healer spends her days by the main gate, helping those rescued from the sands.

Nearby, a large stone stands firmly planted into the ground. Its dark irridescent grey colour contrasts with the etherial blue glow that emanates from it. You have never seen something like this before.

The kind Elanore has helped you get back on your feet. You told her about yourself and she really took a liking to you! She explains that she is a healer and her remedies are highly sought after by the people of Tulimshar.

You seem to have been robbed of most of your possessions, so Elanore offers to pay you to work for her for the day.

After you return with her items, Elanore explains that her healing skills are different from other physicians, as she is able to tap into the energies of the nearby Soul Menhir.

She tells you how there have been recent attacks on the city coming from twisted creatures in the desert, corrupted by Kaore: the opposite force to Mana. She explains that the influence of Kaore has been growing lately and strange things have been happening.

Some people in the city are looking sickly, you may wish to check in on them and then consult with Elanore about their illness. If you help her cure 12 citizens, she will become available to trade x20 [Maggot Slime] for x1 [Potion of Health] {repeatable trade, indefinitely}. If you help her cure 25 citizens she will reward you with {further health items and exp, karma}.

She then directs you to two different NPCs (you may visit them in any order):

- **Apprentice Nina**: Elanore's apprentice. She tells you that she is the one who found you in the desert. She seems to like you and is very concerned for your well-being. She hands you her [Nina's Zielite Amulet] and tells you to return it when you've settled in the city and are no longer in trouble.

She can explain more in depth topics like Mana, Kaore, Zielite, Soul Menhirs and Druids. Listening to her whole dialogue grants {exp}.

She expresses a desire for a sugary treat. You may go to the bakery to get her a croissant. {Gold, exp, karma?}

- **Watchman Ekiru**: Ekiru warns you against going into the desert as unprepared as you were the first time around. He tells you about the troubles the local cactus farmers are having with oversized maggots and suggests that you could get some training by helping the town guard there.

In the cactus fields, Watchman Kyle provides a combat tutorial {full combat tutorial sequence} and a quest. You are tasked with exterminating maggots on the farmers' fields. You must kill 25 maggots {exp, gold}. Killing 50, 100, 250, 500 and 1000 has increasing rewards {exp, gold, special item at 1k}. You are sent back to Watchman Ekiru.

Watchman Eriku is impressed with your work and decides to recommend you for the upcoming expedition to the Old Tulimshar Mines, located outside the city walls and deeper into the desert. You are directed into the guard's barracks where you meet Vigilant Miro, the high ranking officer who will lead the expedition. You are given a basic set of armour and a rusty axe. {Worn Armour set, Rusty Axe}

You are sent outside the city walls to retrieve a lost traveller in the sands. You battle groups of scorpions, sand snakes, serquets and finally a Mister Prickel threatening the nearby lost traveller, who you save {exp, gold, item}. The traveller introduces himself as Julius and tells you that he is a scholar from the Manayir Academy who, by studying ancient lore, has developed a theory about the state of Mana in the world and the importance of bringing back the Mana Trees before Aemil is overrun by Kaore as it was in the previous age. You return Julius to Tulimshar, where he tells you he will stay at the Source of Water Inn and asks you to meet him there when you can.

Vigilant Miro receives your report and is pleased with your efforts. You are directed to the Desert Gate, the southern entrance of Tulimshar, where the expedition party will meet and begin its journey.
