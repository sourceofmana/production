# Production

Administration and organisation of development production and production goals.

## What is this repository for?
This repository contains all development production goals for The Mana World. Anything related to content production should be included here.

---

Issues in this repository are always about pieces of content starting in the concept phase. Issues should be labelled and weighted appropriately by those opening them or by those in charge of production.
 An issue is closed when the content discussed is either completed or discarded.

Production milestones are tracked mainly at group level (outwith this repo) so that any relevant issues and MRs can be assigned to them from various repositories.
 Issues in this repo should be assigned to an appropriate milestone either by those opening them or by those in charge of production.

Any documentation related to production, planning and general organisation should be stored in this repository.

## Work in Progress
Any work in progress content is being discussed across our Miro boards:
- World Design: https://miro.com/app/board/uXjVOh9bBfo=/
- Game Design: https://miro.com/app/board/uXjVPPCT-pY=/

### Contributing to Miro

Miro does not allow users to comment without an upgrade, so if you want to discuss the stuff
on Miro, you must first create an account, and then request edit access.

Please do not edit the maps themselves; There is an option to comment once you have access
to it. Simple points can be discussed briefly in the development channel over IRC or Discord,
but if it is only any complexity, please maintain the discussion as a comment in Miro.

### Contacts
- @WildX
- @Reidy
